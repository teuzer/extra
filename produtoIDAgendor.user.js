// ==UserScript==
// @name         Agendor Visualiza produto ID
// @namespace    http://tampermonkey.net/
// @version      0.1
// @downloadURL  https://bitbucket.org/teuzer/extra/raw/master/produtoIDAgendor.user.js
// @updateURL    https://bitbucket.org/teuzer/extra/raw/master/produtoIDAgendor.user.js
// @description  Exibe o ID dos Fields do Active Campaign
// @author       Teuzer
// @match        https://*.agendor.com.br/sistema/pessoas/inserir.php
// @icon         https://www.google.com/s2/favicons?domain=agendor.com.br
// @grant        none
// ==/UserScript==

(function() {
    setTimeout(function(){
        document.querySelectorAll("#listaProdutos .tag.cuP").forEach(function(elem){
            elem.addEventListener("click",function(){
                let ids = [];
                elem.parentNode.querySelectorAll("#listaProdutos input").forEach(function(el){
                    ids.push(el.value);
                })
                elem.parentNode.previousSibling.innerText = ids.join(" , ");
            });
        });
    },2000);
})();