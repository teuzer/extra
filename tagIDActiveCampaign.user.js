// ==UserScript==
// @name         ActiveCampaign Visualiza Tag ID
// @namespace    http://tampermonkey.net/
// @version      0.1
// @downloadURL  https://bitbucket.org/teuzer/extra/raw/master/tagIDActiveCampaign.user.js
// @updateURL    https://bitbucket.org/teuzer/extra/raw/master/tagIDActiveCampaign.user.js
// @description  Exibe o ID das Tags do Active Campaign
// @author       Teuzer
// @match        https://*.activehosted.com/tags*
// @icon         https://www.google.com/s2/favicons?domain=activehosted.com
// @grant        none
// ==/UserScript==
(function() {
    document.querySelectorAll("#tag_list_table tr").forEach(function(elem){
        let name = elem.querySelector("td.name").innerText;
        elem.querySelector("td.name").innerText = 'ID: '+elem.getAttribute("data-tagid")+' - '+name;
    });
})();