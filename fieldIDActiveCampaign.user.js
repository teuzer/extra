// ==UserScript==
// @name         ActiveCampaign Visualiza Field ID
// @namespace    http://tampermonkey.net/
// @version      0.1
// @downloadURL  https://bitbucket.org/teuzer/extra/raw/master/fieldIDActiveCampaign.user.js
// @updateURL    https://bitbucket.org/teuzer/extra/raw/master/fieldIDActiveCampaign.user.js
// @description  Exibe o ID dos Fields do Active Campaign
// @author       Teuzer
// @match        https://*.activehosted.com/app/settings/fields/contacts*
// @icon         https://www.google.com/s2/favicons?domain=activehosted.com
// @grant        none
// ==/UserScript==
(function() {
    'use strict';
    (function() {
        const send = XMLHttpRequest.prototype.send
        XMLHttpRequest.prototype.send = function() {
            this.addEventListener('load', function() {
                if(this.responseURL.includes("/api/3/fields/")){
                    let id = JSON.parse(this.responseText).field.id;
                    document.querySelector("input.field-group-search").value = id;
                }
            })
            return send.apply(this, arguments)
        }
    })();
})();